﻿using MaterialDesignThemes.Wpf;
using Prism.Ioc;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TreeView.ViewModels;

namespace TreeView.Views
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        IContainerExtension _container;
        public MainWindow(IContainerExtension container)
        {
            InitializeComponent();

            // Prism 컨테이너 초기화
            _container = container;

            MaterialInitialized();
        }

        void MaterialInitialized()
        {
            // 바탕화면 지정
            new PaletteHelper().SetLightDark(true);

            // Prism Container 메인스낵바 등록
            _container.RegisterInstance(MainSnackbar);

            // 컨테이너에 등록된 메인스낵바 테스트
            var snacbara = _container.Resolve<Snackbar>();
            snacbara.MessageQueue.Enqueue("test");
        }
    }
}
