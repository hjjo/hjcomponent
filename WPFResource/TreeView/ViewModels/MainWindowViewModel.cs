﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeView.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private static readonly string _region = "MainRegion";
        public static string region { get => _region; }
    }
}
